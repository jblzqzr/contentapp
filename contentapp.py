from webapp import WebApp
import argparse

diccContent = {'/': './pages/index.html', '/hola': './pages/hola.html', '/adios': './pages/adios.html',
               '/asignatura': './pages/asignatura.html', '/grado': './pages/grado.html',
               '/universidad': './pages/universidad.html'}
PORT = 1234


def parse_args():
    parser = argparse.ArgumentParser(description="Simple HTTP Server")
    parser.add_argument('-p', '--port', type=int, default=PORT,
                        help="TCP port for the server")
    arg = parser.parse_args()
    return arg


class ContentApp(WebApp):
    def parse(self, request):
        parsed_request = request.split()[1]
        return parsed_request

    def process(self, parsed_request):
        if parsed_request in diccContent:
            f = open(diccContent[parsed_request], 'r')
            html = f.read()
            print("Process: Returning 200 OK")
            return "200 OK", html
        else:
            return "404 Not Found", "<html><body><h1>404 Not Found</h1></body></html>"


if __name__ == "__main__":
    args = parse_args()
    app = ContentApp('localhost', args.port)
